<?php

namespace Database\Seeders;

use Database\Factories\BookableFactory;
use Illuminate\Database\Seeder;
use App\Models\Bookable;

class BookableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Bookable::truncate();
        Bookable::factory()->count(10)->create();
    }
}
